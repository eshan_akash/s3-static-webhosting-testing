module "website" {
  source = "./deploy/terraform/static-site"
  
  domain_name = var.domain_name
}
